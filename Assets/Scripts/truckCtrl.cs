﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public class truckInventory
//{
//    float capacity, now_load;
    
//    float value_price;
//}

public class truckCtrl : MonoBehaviour
{
    // Start is called before the first frame update
    //public List<WarehouseInv> m_this_Inventory;
    public WarehouseInv m_this_Inventory;

    public float speed = 0f;
    public float speed_acc = 0f;
    public int direct = 0;
    public GameObject visual, exp_visual;
    void Start()
    {
        updateDir();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * speed * Time.deltaTime);
        //if (direct == 0) // up
        //{
        //    transform.Translate(Vector3.up * speed * Time.deltaTime);
        //}
        //else if (direct == 1) // left
        //{
        //    transform.Translate(Vector3.left * speed * Time.deltaTime);
        //}
        //else if (direct == 2) // down
        //{
        //    transform.Translate(Vector3.up * -speed * Time.deltaTime);
        //}
        //else if (direct == 3) // right
        //{
        //    transform.Translate(Vector3.left * -speed * Time.deltaTime);
        //}
    }

    public void updateDir()
    {
        if (direct == 0)
            this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        else if (direct == 1)
            this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
        else if (direct == 2)
            this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
        else if (direct == 3)
            this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 270f);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    //if (collision.gameObject.tag == "up")
    //    //    direct = 0;
    //    //else if (collision.gameObject.tag == "left")
    //    //    direct = 1;
    //    //else if (collision.gameObject.tag == "down")
    //    //    direct = 2;
    //    //else if (collision.gameObject.tag == "right")
    //    //    direct = 3;
    //    if (collision.gameObject.tag == "up")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 270f);
    //    else if (collision.gameObject.tag == "left")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    //    else if (collision.gameObject.tag == "down")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
    //    else if (collision.gameObject.tag == "right")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
    //    else if (collision.gameObject.tag == "accel")
    //        speed += speed_acc;
    //    else if (collision.gameObject.tag == "deccel")
    //        speed -= speed_acc;
    //}

    //private void OnCollisionStay2D(Collision2D collision)
    //{
    //    if (collision.gameObject.tag == "up")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 270f);
    //    else if (collision.gameObject.tag == "left")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    //    else if (collision.gameObject.tag == "down")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
    //    else if (collision.gameObject.tag == "right")
    //        this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
    //    else if (collision.gameObject.tag == "accel")
    //        speed += speed_acc;
    //    else if (collision.gameObject.tag == "deccel")
    //        speed -= speed_acc;
    //}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "up")
        {
            direct = 0;
            //this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else if (collision.gameObject.tag == "left")
        {
            direct = 1;
        }
        else if (collision.gameObject.tag == "down")
            direct = 2;
        //this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
        else if (collision.gameObject.tag == "right")
            direct = 3;
            //this.gameObject.transform.rotation = Quaternion.Euler(0f, 0f, 270f);
        else if (collision.gameObject.tag == "accel")
            speed += speed_acc;
        else if (collision.gameObject.tag == "deccel")
            speed -= speed_acc;

        else if(collision.gameObject.tag == "truck") //this exploded
        {
            speed = 0f;
            explosion();
            Invoke("delete_this", 1f);
        }

        updateDir();
    }

    public void explosion()
    {
        visual.gameObject.SetActive(false);
        exp_visual.gameObject.SetActive(true);
    }

    public void delete_this()
    {
        Destroy(this.gameObject);
    }
}
