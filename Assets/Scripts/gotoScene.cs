﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gotoScene : MonoBehaviour
{
    public string sceneTarget;
    // Start is called before the first frame update

    public void justGo()
    {
        SceneManager.LoadScene(sceneTarget);
    }
}
