﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneCtrl : MonoBehaviour
{
    public float time = 0f;
    public Text timer_text, remaining_warehouse;
    private int remain_warehouse = 0;
    public GameObject[] wareHouses;
    public GameObject gameOverPanel;

    public bool game_over = false;

    // Start is called before the first frame update
    void Start()
    {
        wareHouses = GameObject.FindGameObjectsWithTag("Pooling");
        //StartCoroutine(updateTime());
    }

    // Update is called once per frame
    void Update()
    {
        checkWarehouse();

        //timer
        if (!game_over)
        {
            if (remain_warehouse <= 0 || time < 0)
                game_over = true;
            if(time > 0) {
                time -= Time.deltaTime;
                timer_text.text = ((int)time).ToString();
            }
        }
        else
        {
            gameOverPanel.SetActive(true);
        }
        //updateTime();
    }

    public void checkWarehouse()
    {
        int cnt = 0;
        for(int i=0; i<wareHouses.Length; i++)
        {
            if (wareHouses[i].GetComponent<Pooling>().health > 1)
                cnt++;
        }
        remain_warehouse = cnt;
        remaining_warehouse.text = remain_warehouse.ToString();
    }

    public IEnumerator updateTime()
    {
        //if (time > 0)
        //{
            time--;
            timer_text.text = time.ToString();
        //}
        //else if (time == 0)
        //    game_over = true;

        yield return new WaitForSeconds(1f);
    }
}
