﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBtn : MonoBehaviour
{
    public GameObject tutorialPanel;
    // Start is called before the first frame update
    public void showTutorial()
    {
        tutorialPanel.gameObject.SetActive(true);
    }
    public void hideTutorial()
    {
        tutorialPanel.gameObject.SetActive(false);
    }
}
