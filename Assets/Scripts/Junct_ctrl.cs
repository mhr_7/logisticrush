﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class directionObj
{
    public Sprite img;
    public int direction_id;
    public string tag_name;
}

[SerializeField]
public class Junct_ctrl : MonoBehaviour
{
    public directionObj[] list_changes;
    public int itt = 0;

    //void Start()
    //{
        
    //}

    // Update is called once per frame
    //void Update()
    //{
        
    //}

    void OnMouseDown()
    {
        int len = list_changes.Length;
        if (itt < len-1)
            itt++;
        else
            itt = 0;

        gameObject.tag = list_changes[itt].tag_name;
        gameObject.GetComponent<SpriteRenderer>().sprite = list_changes[itt].img;

        Debug.Log("clicked");
    }
}
