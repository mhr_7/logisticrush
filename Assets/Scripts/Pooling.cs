﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class WarehouseInv
{
    public string name;
    public float value_price;
    public int qty;
}

[SerializeField]
public class Pooling : MonoBehaviour
{
    public float health = 100;
    public Slider healthBar, loadingBar;
    public Text truck_remain;

    public List<truckCtrl> truck_inside; //Add, Remove
    public List<WarehouseInv> this_inventory, requirement;

    public float initSpeed = 0f, initAcc = 0f;
    public int initDir = 0;
    public Transform initPos, comingPos;
    public GameObject truckPrefab, explosion, visual;

    public float loadingSpeed = 0f;
    private bool isReady = false;
    private float loadingProgress = 0f;

    public float revenue = 0f;

    // Start is called before the first frame update
    void Start()
    {
        healthBar.value = 100;
        truck_remain.text = this_inventory.Count.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isReady)
        {
            loadingBar.value = loadingProgress;
            loadingProgress += loadingSpeed;
            if(loadingProgress >= 100f)
            {
                isReady = true;
                loadingBar.gameObject.SetActive(false);
            }
        }
    }

    private void OnMouseUp()
    {
        //logistic rush mode -----------------------------------------------------
        //GameObject truck = Instantiate(truckPrefab, initPos.position, Quaternion.identity) as GameObject;
        //truck.gameObject.GetComponent<truckCtrl>().m_this_Inventory = this_inventory[0];
        //truck.gameObject.GetComponent<truckCtrl>().direct = initDir;
        //truck.gameObject.GetComponent<truckCtrl>().speed = initSpeed;
        //truck.gameObject.GetComponent<truckCtrl>().speed_acc = initAcc;
        //this_inventory.RemoveAt(0);
        //truck_inside.RemoveAt(0);

        //Battle Warehouse mode --------------------------------------------------
        if (isReady && this_inventory.Count > 0 && health > 0)
        {
            GameObject truck = Instantiate(truckPrefab, initPos.position, Quaternion.identity) as GameObject;
            truck.gameObject.GetComponent<truckCtrl>().m_this_Inventory = this_inventory[0];
            truck.gameObject.GetComponent<truckCtrl>().direct = initDir;
            truck.gameObject.GetComponent<truckCtrl>().speed = initSpeed;
            truck.gameObject.GetComponent<truckCtrl>().speed_acc = initAcc;

            //range
            //0-10
            //11-20
            //21-30
            //30 ++
            if(this_inventory[0].value_price > 0 && this_inventory[0].value_price <= 10)
            {
                //truck.gameObject.GetComponent<SpriteRenderer>().color = new Color(100f, 110f, 100f);
                truck.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.white;
            }else if (this_inventory[0].value_price > 10 && this_inventory[0].value_price <= 20)
            {
                //truck.gameObject.GetComponent<SpriteRenderer>().color = new Color(100f, 110f, 100f);
                truck.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.blue;
            }else if (this_inventory[0].value_price > 20 && this_inventory[0].value_price <= 30)
            {
                //truck.gameObject.GetComponent<SpriteRenderer>().color = new Color(100f, 110f, 100f);
                truck.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.yellow;
            }else if (this_inventory[0].value_price > 30)
            {
                //truck.gameObject.GetComponent<SpriteRenderer>().color = new Color(100f, 110f, 100f);
                truck.gameObject.GetComponentInChildren<SpriteRenderer>().color = Color.black;
            }

            this_inventory.RemoveAt(0);

            truck_remain.text = this_inventory.Count.ToString();

            isReady = false;
            loadingProgress = 0f;
            loadingBar.gameObject.SetActive(true);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "truck")
        {
            //Battle Warehouse mode -------------------------------------------------------------------
            DestroyObject(collision.gameObject);
            health -= collision.gameObject.GetComponent<truckCtrl>().m_this_Inventory.value_price;
            healthBar.value = health;

            //Logistic Rush mode -----------------------------------------------------------------------
            //bool flag = false;
            //for(int i= 0; i<requirement.Count; i++)
            //{
            //    if (requirement[i].name == collision.GetComponent<truckCtrl>().m_this_Inventory.name)
            //    {
            //        flag = true;
            //        revenue += collision.GetComponent<truckCtrl>().m_this_Inventory.value_price;
            //        requirement.RemoveAt(i);
            //        break;
            //    }
            //}
            //if (flag)//accept them, moving on
            //{
            //    truck_inside.Add(collision.gameObject.GetComponent<truckCtrl>());
            //    Destroy(collision.gameObject);
            //}
            //else //reject them, go away
            //{
            //    if (collision.gameObject.GetComponent<truckCtrl>().direct == 0)
            //    {
            //        collision.gameObject.GetComponent<truckCtrl>().direct = 2;
            //    } else if (collision.gameObject.GetComponent<truckCtrl>().direct == 1)
            //    {
            //        collision.gameObject.GetComponent<truckCtrl>().direct = 3;
            //    } else if (collision.gameObject.GetComponent<truckCtrl>().direct == 2) 
            //    {
            //        collision.gameObject.GetComponent<truckCtrl>().direct = 0;
            //    } else if (collision.gameObject.GetComponent<truckCtrl>().direct == 3)
            //    {
            //        collision.gameObject.GetComponent<truckCtrl>().direct = 1;
            //    }
            //    collision.gameObject.transform.position = comingPos.transform.position;
            //    collision.gameObject.GetComponent<truckCtrl>().updateDir();
            //}

        }

        if(health < 0)
        {
            visual.gameObject.SetActive(false);
            explosion.gameObject.SetActive(true);
        }
    }
}
